import { ArrowBack, EditOutlined } from "@mui/icons-material";
import { Button, Divider, Grid, Typography } from "@mui/material";
import { ThunkDispatch } from "@reduxjs/toolkit";
import EventDetail from "components/EventDetail/EventDetail";
import EventForm from "components/EventForm/EventForm";
import Modal from "components/Modal/Modal";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { RootState } from "store";
import { fetchAllEvents } from "store/eventSlice";
import * as S from "./EventDetailScreen.styles";

const EventDetailScreen: React.FC = () => {
  const [open, setOpen] = useState(false);
  const routeParams = useParams<{ eventId: string }>();
  const eventId = routeParams.eventId as string;
  const dispatch = useDispatch<ThunkDispatch<RootState, undefined, any>>();
  const { events } = useSelector((state: RootState) => state.events);

  const event = events?.[eventId];

  useEffect(() => {
    dispatch(fetchAllEvents());
  }, []);

  const handleClose = () => {
    setOpen(false);
  };

  if (!event) {
    return null;
  }

  return (
    <Grid item xs={12} md={8}>
      <Modal open={open} handleCloseClick={handleClose} title="Update event">
        <EventForm handleCancel={handleClose} defaultValues={event} />
      </Modal>
      <S.HeaderWrapper>
        <Typography variant="h4" component="h1" gutterBottom mb={0}>
          Event detail
        </Typography>
        <Link to="/">
          <Button variant="contained" startIcon={<ArrowBack />} size="large">
            Back to list
          </Button>
        </Link>
      </S.HeaderWrapper>
      <Divider />
      <EventDetail event={event} />
      <Button
        variant="contained"
        onClick={() => setOpen(true)}
        startIcon={<EditOutlined />}
      >
        Edit event
      </Button>
    </Grid>
  );
};

export default EventDetailScreen;
