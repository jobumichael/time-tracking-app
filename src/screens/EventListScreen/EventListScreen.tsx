import { Add } from "@mui/icons-material";
import {
  DialogActions,
  DialogContent,
  DialogContentText,
  Divider,
  Grid,
  Typography,
} from "@mui/material";
import Button from "@mui/material/Button";
import { ThunkDispatch } from "@reduxjs/toolkit";
import EventForm from "components/EventForm/EventForm";
import EventList from "components/EventList/EventList";
import Modal from "components/Modal/Modal";
import { IEvent } from "interfaces";
import * as React from "react";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "store";
import { deleteEvent, fetchAllEvents } from "store/eventSlice";
import { getSortedEvents } from "util/getSortedEvents";
import * as S from "./EventListScreen.styles";

const EventListScreen = () => {
  const [open, setOpen] = React.useState(false);
  const [editEventData, setEditEventData] = React.useState<IEvent | null>(null);
  const [deleteConfirmOpen, setDeleteConfirmOpen] = React.useState(false);
  const deleteEventId = React.useRef<string | null>(null);

  const dispatch = useDispatch<ThunkDispatch<RootState, undefined, any>>();
  const { events } = useSelector((state: RootState) => state.events);

  useEffect(() => {
    dispatch(fetchAllEvents());
  }, []);

  const handleClose = () => {
    setOpen(false);
    setEditEventData(null);
  };

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleEditEvent = (event: IEvent) => {
    setEditEventData(event);
    setOpen(true);
  };

  const handleDeleteEvent = (id: string) => {
    setDeleteConfirmOpen(true);
    deleteEventId.current = id;
  };

  const handleDeleteConfirm = () => {
    dispatch(deleteEvent(deleteEventId.current as string));
    setDeleteConfirmOpen(false);
  };

  const sortedEvents = getSortedEvents(Object.values(events || {}));

  return (
    <div>
      <Modal
        open={open}
        handleCloseClick={handleClose}
        title={editEventData ? "Update event" : "Create new event"}
      >
        <EventForm handleCancel={handleClose} defaultValues={editEventData} />
      </Modal>
      <Modal
        open={deleteConfirmOpen}
        handleCloseClick={() => setDeleteConfirmOpen(false)}
      >
        <DialogContent>
          <DialogContentText>
            Are you sure you want to delete this event?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button
            variant="outlined"
            onClick={() => setDeleteConfirmOpen(false)}
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            color="error"
            onClick={handleDeleteConfirm}
          >
            Delete
          </Button>
        </DialogActions>
      </Modal>

      <S.HeaderWrapper>
        <Typography variant="h4" component="h1" gutterBottom mb={0}>
          All events
        </Typography>
        <Button variant="contained" endIcon={<Add />} onClick={handleClickOpen}>
          Create new event
        </Button>
      </S.HeaderWrapper>
      <Divider />
      <S.EventListWrapper>
        {sortedEvents.length > 0 ? (
          <EventList
            events={sortedEvents}
            onEditEvent={handleEditEvent}
            onDeleteEvent={handleDeleteEvent}
          />
        ) : (
          <Grid
            container
            justifyContent="center"
            alignItems="center"
            height="100%"
          >
            <Grid item>
              <Typography variant="h5" component="h2" gutterBottom>
                No events yet, start by creating one!
              </Typography>
            </Grid>
          </Grid>
        )}
      </S.EventListWrapper>
    </div>
  );
};

export default EventListScreen;
