import { render, screen } from "@testing-library/react";
import KeyValueDisplay from "./KeyValueDisplay";

describe("KeyValueDisplay", () => {
  it("renders the key name and value", () => {
    const keyName = "Name";
    const value = "John Doe";

    render(<KeyValueDisplay keyName={keyName} value={value} />);

    expect(screen.getByText("Name :")).toBeInTheDocument();
    expect(screen.getByText(value)).toBeInTheDocument();
  });
});
