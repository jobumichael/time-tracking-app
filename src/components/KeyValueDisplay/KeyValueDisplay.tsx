import Typography from "@mui/material/Typography";
import React from "react";
import * as S from "./KeyValueDisplay.styles";

interface IProps {
  keyName: string;
  value: string;
}

const KeyValueDisplay: React.FC<IProps> = ({ keyName, value }) => {
  return (
    <S.Wrapper display="flex" alignItems="center">
      <Typography
        variant="subtitle1"
        component="p"
      >{`${keyName} : `}</Typography>

      <Typography variant="body1" component="span">
        <b>{value}</b>
      </Typography>
    </S.Wrapper>
  );
};

export default KeyValueDisplay;
