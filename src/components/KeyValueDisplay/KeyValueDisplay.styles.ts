import styled from "@emotion/styled";
import { Box } from "@mui/material";

export const Wrapper = styled(Box)`
  & p,
  b {
    margin-right: 5px;
    margin: 5px 5px 5px 0;
  }
`;
