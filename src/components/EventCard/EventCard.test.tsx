import { fireEvent, render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import EventCard from "./EventCard";

const mockEvent = {
  id: "1",
  eventName: "Sample Event",
  startTime: "2023-10-16T09:00:00",
  endTime: "2023-10-16T12:00:00",
  color: "blue",
  description: "Sample description",
};

const mockOnEditEvent = jest.fn();
const mockOnDeleteEvent = jest.fn();

it("EventCard renders with correct event details", () => {
  render(
    <Router>
      <EventCard
        event={mockEvent}
        onEditEvent={mockOnEditEvent}
        onDeleteEvent={mockOnDeleteEvent}
      />
    </Router>
  );

  expect(screen.getByText("Sample Event")).toBeInTheDocument();
  expect(screen.getByText("09:00 AM")).toBeInTheDocument();
  expect(screen.getByText("12:00 PM")).toBeInTheDocument();
  expect(screen.getByText("3 hr")).toBeInTheDocument();
});

it("Edit button calls onEditEvent callback", () => {
  render(
    <Router>
      <EventCard
        event={mockEvent}
        onEditEvent={mockOnEditEvent}
        onDeleteEvent={mockOnDeleteEvent}
      />
    </Router>
  );

  const editButton = screen.getByText("Edit");
  fireEvent.click(editButton);

  expect(mockOnEditEvent).toHaveBeenCalledWith(mockEvent);
});

it("Delete button calls onDeleteEvent callback", () => {
  render(
    <Router>
      <EventCard
        event={mockEvent}
        onEditEvent={mockOnEditEvent}
        onDeleteEvent={mockOnDeleteEvent}
      />
    </Router>
  );

  const deleteButton = screen.getByText("Delete");
  fireEvent.click(deleteButton);

  expect(mockOnDeleteEvent).toHaveBeenCalledWith("1");
});
