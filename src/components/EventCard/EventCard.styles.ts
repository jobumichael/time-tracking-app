import styled from "@emotion/styled";
import { Card } from "@mui/material";

export const CardWrapper = styled(Card)`
  display: flex;
  flex-direction: column;
  height: 100%;
  position: relative;
  width: 280px;

  & h6 {
    height: 58px;
    display: -webkit-box;
    -webkit-line-clamp: 2;
    -webkit-box-orient: vertical;
    overflow: hidden;
  }
`;

export const ColorBar = styled.div<{ color: string }>`
  background-color: ${({ color }) => color};
  height: 5px;
  width: 100%;
`;
