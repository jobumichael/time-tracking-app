import { DeleteOutline, EditOutlined } from "@mui/icons-material";
import { CardActionArea } from "@mui/material";
import Button from "@mui/material/Button";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import KeyValueDisplay from "components/KeyValueDisplay/KeyValueDisplay";
import dayjs from "dayjs";
import { IEvent } from "interfaces";
import { Link } from "react-router-dom";
import { calculateDuration } from "util/calculateDuration";
import { getFormattedTime } from "util/date";
import * as S from "./EventCard.styles";

interface IProps {
  event: IEvent;
  onEditEvent: (event: IEvent) => void;
  onDeleteEvent: (id: string) => void;
}

const EventCard: React.FC<IProps> = ({ event, onEditEvent, onDeleteEvent }) => {
  const { eventName, startTime, endTime, color, id } = event;
  const duration = calculateDuration(
    dayjs(startTime).toDate(),
    dayjs(endTime).toDate()
  );

  const handleDeleteEvent = (e: React.MouseEvent<HTMLButtonElement>) => {
    onDeleteEvent(id);
  };

  const handleEditEvent = (e: React.MouseEvent<HTMLButtonElement>) => {
    onEditEvent(event);
  };

  return (
    <S.CardWrapper>
      <S.ColorBar color={color} />
      <Link to={`/event/${id}`}>
        <CardActionArea>
          <CardContent sx={{ flexGrow: 1 }}>
            <Typography gutterBottom variant="h6">
              {eventName}
            </Typography>
            <KeyValueDisplay
              keyName="Start time"
              value={getFormattedTime(startTime)}
            />
            <KeyValueDisplay
              keyName="End time"
              value={getFormattedTime(endTime)}
            />
            <KeyValueDisplay keyName="Duration" value={duration} />
          </CardContent>
        </CardActionArea>
      </Link>
      <CardActions>
        <Button
          size="small"
          variant="contained"
          color="error"
          onClick={handleDeleteEvent}
          startIcon={<DeleteOutline />}
        >
          Delete
        </Button>
        <Button
          variant="contained"
          size="small"
          onClick={handleEditEvent}
          startIcon={<EditOutlined />}
        >
          Edit
        </Button>
      </CardActions>
    </S.CardWrapper>
  );
};

export default EventCard;
