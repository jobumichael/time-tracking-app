import { Cancel, Save } from "@mui/icons-material";
import { FormControl, FormHelperText } from "@mui/material";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { LocalizationProvider, TimePicker } from "@mui/x-date-pickers";
import { AdapterDayjs } from "@mui/x-date-pickers/AdapterDayjs";
import ColorPicker from "components/ColorPicker/ColorPicker";
import dayjs, { Dayjs } from "dayjs";
import { IEvent } from "interfaces";
import * as React from "react";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addEvent } from "store/eventSlice";
import { getRandomId } from "util/getRandomId";
import { validateFormData } from "util/validateFormData";

interface IProps {
  defaultValues?: IEvent | null;
  handleCancel: () => void;
}

const EventForm: React.FC<IProps> = ({ defaultValues, handleCancel }) => {
  const [submitting, setSubmitting] = useState(false);
  const [formData, setFormData] = useState({
    eventName: defaultValues?.eventName || "",
    startTime: defaultValues?.startTime || undefined,
    endTime: defaultValues?.endTime || undefined,
    description: defaultValues?.description || "",
    color: defaultValues?.color || "Black",
  });
  const dispatch = useDispatch();
  const isEdit = !!defaultValues?.id;

  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    setSubmitting(true);

    const hasError = validateFormData(formData);
    if (hasError) {
      return;
    }

    const id = isEdit ? defaultValues?.id : getRandomId();
    const newEvent = {
      ...formData,
      id,
    };
    dispatch(addEvent(newEvent));
    handleCancel();
  };

  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setFormData({ ...formData, [event.target.name]: event.target.value });
  };

  const handleTimeChange = (name: string, time: Dayjs | null) => {
    setFormData({ ...formData, [name]: time?.toString() });
  };

  const handleColorChange = (color: string) => {
    setFormData({ ...formData, color });
  };

  return (
    <Box
      sx={{
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
      }}
    >
      <Box component="form" noValidate onSubmit={handleSubmit} sx={{ mt: 3 }}>
        <Grid container spacing={4}>
          <Grid item xs={12}>
            <FormControl fullWidth error={true}>
              <TextField
                defaultValue={defaultValues?.eventName}
                fullWidth
                id="eventName"
                label="Event name *"
                name="eventName"
                value={formData.eventName}
                onChange={handleInputChange}
              />
              {!formData.eventName && submitting && (
                <FormHelperText>Event name is required</FormHelperText>
              )}
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl fullWidth error={true}>
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  label="Start time *"
                  defaultValue={
                    defaultValues?.startTime
                      ? dayjs(defaultValues.startTime)
                      : undefined
                  }
                  onChange={(time) => handleTimeChange("startTime", time)}
                />
              </LocalizationProvider>
              {!formData.startTime && submitting && (
                <FormHelperText>Start time is required</FormHelperText>
              )}
            </FormControl>
          </Grid>
          <Grid item xs={12} sm={6}>
            <FormControl fullWidth error={true} data-testid="end-time">
              <LocalizationProvider dateAdapter={AdapterDayjs}>
                <TimePicker
                  defaultValue={
                    defaultValues?.endTime
                      ? dayjs(defaultValues.endTime)
                      : undefined
                  }
                  onChange={(time) => handleTimeChange("endTime", time)}
                  label="End time *"
                />
              </LocalizationProvider>
              {!formData.endTime && submitting && (
                <FormHelperText>End time is required</FormHelperText>
              )}
              {formData.endTime &&
                formData.startTime &&
                dayjs(formData.startTime).isAfter(dayjs(formData.endTime)) && (
                  <FormHelperText>
                    End time must be greater than start time
                  </FormHelperText>
                )}
            </FormControl>
          </Grid>

          <Grid item xs={12}>
            <TextField
              fullWidth
              id="description"
              label="Description"
              name="description"
              multiline
              rows={3}
              value={formData.description}
              onChange={(e) =>
                setFormData({ ...formData, description: e.target.value })
              }
            />
          </Grid>
          <Grid item xs={12}>
            <ColorPicker
              onColorChange={handleColorChange}
              defaultColor={defaultValues?.color}
            />
          </Grid>
        </Grid>
        <Grid container sx={{ mt: "30px" }}>
          <Button type="submit" variant="contained" startIcon={<Save />}>
            {isEdit ? "Update event" : "Create event"}
          </Button>
          <Button
            onClick={handleCancel}
            startIcon={<Cancel />}
            variant="outlined"
            sx={{ ml: "10px" }}
          >
            Cancel
          </Button>
        </Grid>
      </Box>
    </Box>
  );
};

export default EventForm;
