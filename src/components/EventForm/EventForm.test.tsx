// @ts-nocheck
import { fireEvent, render, screen } from "@testing-library/react";
import { Provider } from "react-redux";
import configureStore from "redux-mock-store";
import EventForm from "./EventForm";

const mockStore = configureStore([]);

const mockDispatch = jest.fn();

const defaultValues = {
  eventName: "Test Event",
  startTime: null,
  endTime: null,
  description: "Test Description",
  color: "Blue",
  id: null,
};

const initialState = {
  events: [],
};

describe("EventForm Component", () => {
  let store;

  beforeEach(() => {
    store = mockStore(initialState);
    store.dispatch = mockDispatch;
  });

  it("renders EventForm component", () => {
    render(
      <Provider store={store}>
        <EventForm defaultValues={defaultValues} handleCancel={() => {}} />
      </Provider>
    );

    expect(screen.getByLabelText("Event name *")).toBeInTheDocument();
    expect(screen.getByLabelText("Start time *")).toBeInTheDocument();
    expect(screen.getByLabelText("End time *")).toBeInTheDocument();
    expect(screen.getByLabelText("Description")).toBeInTheDocument();
    expect(screen.getByText("Create event")).toBeInTheDocument();
    expect(screen.getByText("Cancel")).toBeInTheDocument();
  });
  it("should not submit form if required fields are empty", () => {
    render(
      <Provider store={store}>
        <EventForm defaultValues={defaultValues} handleCancel={() => {}} />
      </Provider>
    );

    const saveButton = screen.getByRole("button", { name: "Create event" });

    fireEvent.click(saveButton);

    expect(mockDispatch).not.toHaveBeenCalled();
  });

  it("handles form submission", () => {
    render(
      <Provider store={store}>
        <EventForm defaultValues={defaultValues} handleCancel={() => {}} />
      </Provider>
    );

    const titleInput = screen.getByLabelText("Event name *");
    const saveButton = screen.getByRole("button", { name: "Create event" });
    const descriptionInput = screen.getByLabelText("Description");

    fireEvent.change(titleInput, { target: { value: "Test Event" } });
    fireEvent.change(descriptionInput, {
      target: { value: "Test Description" },
    });
    fireEvent.click(saveButton);

    expect(titleInput).toHaveValue("Test Event");
    expect(descriptionInput).toHaveValue("Test Description");
  });
});
