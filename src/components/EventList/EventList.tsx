import Grid from "@mui/material/Grid";
import EventCard from "components/EventCard/EventCard";
import { IEvent } from "interfaces";

interface IProps {
  events: Array<IEvent>;
  onEditEvent: (event: IEvent) => void;
  onDeleteEvent: (id: string) => void;
}

const EventList: React.FC<IProps> = ({
  events,
  onEditEvent,
  onDeleteEvent,
}) => {
  return (
    <>
      <Grid container spacing={4}>
        {events.map((event) => (
          <Grid item key={event.id} xs={12} sm={6} md={4}>
            <EventCard
              data-testid="event-card"
              event={event}
              onEditEvent={onEditEvent}
              onDeleteEvent={onDeleteEvent}
            />
          </Grid>
        ))}
      </Grid>
    </>
  );
};

export default EventList;
