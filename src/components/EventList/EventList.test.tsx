import { render, screen } from "@testing-library/react";
import { BrowserRouter as Router } from "react-router-dom";
import EventList from "./EventList";

const mockEvents = [
  {
    id: "1",
    eventName: "Event 1",
    startTime: "2023-10-16T09:00:00",
    endTime: "2023-10-16T12:00:00",
    color: "blue",
    description: "Description 1",
  },
  {
    id: "2",
    eventName: "Event 2",
    startTime: "2023-10-16T14:00:00",
    endTime: "2023-10-16T16:00:00",
    color: "red",
    description: "Description 2",
  },
];

it("renders EventList component with event cards", () => {
  render(
    <Router>
      <EventList
        events={mockEvents}
        onEditEvent={() => {}}
        onDeleteEvent={() => {}}
      />
    </Router>
  );

  const allLinks = screen.getAllByRole("link");
  expect(allLinks).toHaveLength(mockEvents.length);

  mockEvents.forEach((event, index) => {
    const eventCard = allLinks[index];

    expect(screen.getByText(event.eventName)).toBeInTheDocument();
    expect(eventCard).toHaveAttribute("href", `/event/${event.id}`);
  });
});
