import styled from "@emotion/styled";

export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  height: 100%;
  margin: 2rem 0;
  & p,
  b {
    margin-right: 5px;
    margin: 5px 5px 5px 0;
  }
`;

export const ColorSquare = styled.div(({ color }) => ({
  width: "30px",
  height: "30px",
  backgroundColor: color,
  display: "inline-block",
  marginRight: "10px",
  float: "right",

  "@media (max-width: 768px)": {
    float: "none",
  },
}));

export const DescWrapper = styled.div`
  margin-top: 1rem;

  & h6 {
    margin-top: 15px;
  }
`;
