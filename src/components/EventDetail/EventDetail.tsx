import { Divider, Grid, Typography } from "@mui/material";
import KeyValueDisplay from "components/KeyValueDisplay/KeyValueDisplay";
import dayjs from "dayjs";
import { IEvent } from "interfaces";
import React from "react";
import { calculateDuration } from "util/calculateDuration";
import { getFormattedDate } from "util/date";
import * as S from "./EventDetail.styles";

interface IProps {
  event: IEvent;
}

const EventDetail: React.FC<IProps> = ({ event }) => {
  const { startTime, endTime } = event;
  const duration = calculateDuration(
    dayjs(startTime).toDate(),
    dayjs(endTime).toDate()
  );

  return (
    <S.Wrapper>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={6}>
          <Typography variant="h6" gutterBottom>
            {event.eventName}
          </Typography>
        </Grid>
        <Grid item xs={12} sm={6}>
          <S.ColorSquare data-testid="color-square" color={event.color} />
        </Grid>
      </Grid>

      <KeyValueDisplay
        keyName="Start time"
        value={getFormattedDate(startTime)}
      />
      <KeyValueDisplay keyName="End time" value={getFormattedDate(endTime)} />
      <KeyValueDisplay keyName="Duration" value={duration} />

      {event.description && (
        <S.DescWrapper>
          <Divider />
          <Typography variant="h6" gutterBottom>
            Description
          </Typography>
          <Typography variant="body1" className="markdown">
            {event.description}
          </Typography>
        </S.DescWrapper>
      )}
    </S.Wrapper>
  );
};

export default EventDetail;
