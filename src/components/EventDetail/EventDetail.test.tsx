import { render, screen } from "@testing-library/react";
import { IEvent } from "interfaces";
import EventDetail from "./EventDetail";

describe("EventDetail", () => {
  const mockEvent: IEvent = {
    id: "1",
    eventName: "Sample event",
    startTime: "2022-01-01T10:00:00.000Z",
    endTime: "2022-01-01T12:00:00.000Z",
    color: "blue",
    description: "Sample description",
  };

  it("renders the event color square with the correct color", () => {
    render(<EventDetail event={mockEvent} />);

    const colorSquare = screen.getByTestId("color-square");
    expect(colorSquare).toHaveStyle({ backgroundColor: "blue" });
  });

  it("renders the start time with the correct format", () => {
    render(<EventDetail event={mockEvent} />);

    expect(
      screen.getByText("Sat, January 1, 2022, 11:00 AM")
    ).toBeInTheDocument();
  });

  it("renders the end time with the correct format", () => {
    render(<EventDetail event={mockEvent} />);

    expect(
      screen.getByText("Sat, January 1, 2022, 11:00 AM")
    ).toBeInTheDocument();
  });

  it("renders the duration with the correct value", () => {
    render(<EventDetail event={mockEvent} />);

    expect(screen.getByText("2 hr")).toBeInTheDocument();
  });

  it("renders the description when it is present", () => {
    render(<EventDetail event={mockEvent} />);

    expect(screen.getByText("Description")).toBeInTheDocument();
    expect(screen.getByText("Sample description")).toBeInTheDocument();
  });

  it("does not render the description when it is not present", () => {
    const mockEventWithoutDescription = {
      ...mockEvent,
      description: "",
    };
    render(<EventDetail event={mockEventWithoutDescription} />);

    expect(screen.queryByText("Description")).not.toBeInTheDocument();
    expect(screen.queryByText("Sample description")).not.toBeInTheDocument();
  });
});
