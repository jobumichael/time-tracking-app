import styled from "@emotion/styled";

export const ColorCircle = styled("div")(({ color }) => ({
  width: "20px",
  height: "20px",
  backgroundColor: color,
  display: "inline-block",
  marginRight: "10px",
  borderRadius: "50%",
}));

export const ColorItem = styled("div")`
  display: flex;
  align-items: center;
`;
