import { render, screen } from "@testing-library/react";
import ColorPicker from "./ColorPicker";

const mockOnColorChange = jest.fn();

describe("ColorPicker", () => {
  it("renders with default color and options", () => {
    render(
      <ColorPicker onColorChange={mockOnColorChange} defaultColor="Red" />
    );

    expect(screen.getByTestId("color-label")).toBeInTheDocument();
    expect(screen.getByText("Red")).toBeInTheDocument();
  });
});
