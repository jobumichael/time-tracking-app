import {
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from "@mui/material";
import { colors } from "config/colors";
import { useState } from "react";
import * as S from "./ColorPicker.styles";

interface IProps {
  onColorChange: (color: string) => void;
  defaultColor: string | undefined;
}

const ColorPicker: React.FC<IProps> = ({ onColorChange, defaultColor }) => {
  const [selectedColor, setSelectedColor] = useState<string>(
    defaultColor || ""
  );

  const handleColorChange = (event: SelectChangeEvent) => {
    const color = event.target.value as string;
    setSelectedColor(color);
    onColorChange(color);
  };

  return (
    <FormControl>
      <InputLabel id="colorSelector" data-testid="color-label">
        Event color
      </InputLabel>
      <Select
        labelId="colorSelector"
        value={selectedColor}
        label="Event color"
        onChange={handleColorChange}
        sx={{
          minWidth: "200px",
        }}
        data-testid="color-selector"
      >
        {colors.map((color) => (
          <MenuItem key={color.value} value={color.label}>
            <S.ColorItem>
              <S.ColorCircle color={color.value} />
              <span>{color.label}</span>
            </S.ColorItem>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default ColorPicker;
