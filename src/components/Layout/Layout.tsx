import { Container, CssBaseline } from "@mui/material";
import Footer from "components/Footer/Footer";
import Header from "components/Header/Header";
import React from "react";

interface IProps {
  children: React.ReactNode;
}

const Layout: React.FC<IProps> = ({ children }) => {
  return (
    <>
      <CssBaseline />
      <Header />
      <main>
        <Container sx={{ py: 8 }} maxWidth="md">
          {children}
        </Container>
      </main>
      <Footer />
    </>
  );
};

export default Layout;
