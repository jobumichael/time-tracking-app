import { Dialog, DialogContent, DialogTitle } from "@mui/material";

interface IProps {
  open: boolean;
  handleCloseClick: () => void;
  children: React.ReactNode;
  title?: string;
}

const Modal: React.FC<IProps> = ({
  open,
  handleCloseClick,
  children,
  title,
}) => {
  return (
    <Dialog
      open={open}
      onClose={handleCloseClick}
      maxWidth="md"
      style={{
        padding: "30px",
      }}
    >
      {title && <DialogTitle>{title}</DialogTitle>}
      <DialogContent sx={{ padding: "50px" }}>{children}</DialogContent>
    </Dialog>
  );
};

export default Modal;
