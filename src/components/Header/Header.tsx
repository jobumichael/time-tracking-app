import DateRange from "@mui/icons-material/DateRange";
import { AppBar, Toolbar, Typography } from "@mui/material";

const Header = () => {
  return (
    <AppBar position="relative">
      <Toolbar>
        <DateRange sx={{ mr: 2 }} />
        <Typography variant="h6" color="inherit" noWrap>
          Time tracking app
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Header;
