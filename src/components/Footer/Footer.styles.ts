import styled from "@emotion/styled";

export const Wrapper = styled("div")`
  background-color: #f5f5f5;
  position: fixed;
  bottom: 0;
  text-align: center;
  width: 100%;
  padding: 10px 0;
`;
