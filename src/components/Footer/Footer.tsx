import { Typography } from "@mui/material";
import * as S from "./Footer.styles";

const Footer = () => {
  return (
    <S.Wrapper>
      <Typography
        variant="subtitle1"
        align="center"
        color="text.secondary"
        component="p"
      >
        Created by Jobe
      </Typography>
    </S.Wrapper>
  );
};

export default Footer;
