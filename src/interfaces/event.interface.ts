export interface IEvent {
  id: string;
  eventName: string;
  startTime: string;
  endTime: string;
  duration?: string;
  description: string;
  color: string;
}
