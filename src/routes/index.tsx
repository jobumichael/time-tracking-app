import { BrowserRouter, Route, Routes } from "react-router-dom";
import { EventDetailScreen, EventListScreen } from "screens";
import NotFoundScreen from "screens/NotFoundScreen";

export const AppRoutes = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<EventListScreen />} />
        <Route path="/event/:eventId" element={<EventDetailScreen />} />
        <Route path="*" element={<NotFoundScreen />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRoutes;
