import { configureStore } from "@reduxjs/toolkit";
import { combineReducers } from "redux";
import eventSlice from "./eventSlice";

const rootReducer = combineReducers({
  events: eventSlice,
});

const store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof store.getState>;
export default store;
