import { createSlice } from "@reduxjs/toolkit";
import { IEvent } from "interfaces";
import { getLocalStorageItem, setLocalStorageItem } from "util/localStorage";

interface EventState {
  events: Record<string, IEvent> | null;
}

const initialState: EventState = {
  events: null,
};

const eventSlice = createSlice({
  name: "events",
  initialState,
  reducers: {
    addEvent: (state, action) => {
      if (!state.events) {
        state.events = {};
      }
      state.events[action.payload.id] = action.payload;
      setLocalStorageItem("events", {
        ...state.events,
        [action.payload.id]: action.payload,
      });
    },
    deleteEvent: (state, action) => {
      if (!state.events) {
        return;
      }
      delete state.events[action.payload];
      setLocalStorageItem("events", state.events);
    },
    fetchAllEvents: (state) => {
      const allEvents = getLocalStorageItem("events");

      state.events = allEvents as Record<string, IEvent>;
    },
  },
});

export const { addEvent, deleteEvent, fetchAllEvents } = eventSlice.actions;

export default eventSlice.reducer;
