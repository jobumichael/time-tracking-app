// @ts-nocheck
import reducer, { addEvent, deleteEvent, fetchAllEvents } from "./eventSlice";

describe("eventSlice reducer", () => {
  it("should handle adding an event", () => {
    const initialState = {
      events: null,
    };
    const event = { id: "1", name: "Test Event" };
    const action = addEvent(event);
    const newState = reducer(initialState, action);

    expect(newState.events).toEqual({ "1": event });
  });

  it("should handle deleting an event", () => {
    const initialState = {
      events: {
        "1": {
          id: "1",
          eventName: "Sample event",
          startTime: "2022-01-01T10:00:00.000Z",
          endTime: "2022-01-01T12:00:00.000Z",
          color: "blue",
          description: "Sample description",
        },
      },
    };

    const action = deleteEvent("1");
    const newState = reducer(initialState, action);

    expect(newState.events).toEqual({});
  });

  it("should handle fetching all events", () => {
    const initialState = {
      events: null,
    };
    const storedEvents = {
      "1": {
        id: "1",
        eventName: "Sample event",
        startTime: "2022-01-01T10:00:00.000Z",
        endTime: "2022-01-01T12:00:00.000Z",
        color: "blue",
        description: "Sample description",
      },
    };

    localStorage.setItem("events", JSON.stringify(storedEvents));

    const action = fetchAllEvents();
    const newState = reducer(initialState, action);

    expect(newState.events).toEqual(storedEvents);
  });
});
