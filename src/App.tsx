import { ThemeProvider } from "@emotion/react";
import Layout from "components/Layout/Layout";
import { Provider } from "react-redux";
import AppRoutes from "routes";
import store from "store";
import { customTheme } from "styles/theme";

function App() {
  return (
    <ThemeProvider theme={customTheme}>
      <Provider store={store}>
        <Layout>
          <AppRoutes />
        </Layout>
      </Provider>
    </ThemeProvider>
  );
}

export default App;
