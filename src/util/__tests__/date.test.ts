import { getFormattedDate, getFormattedTime } from "../date";

describe("getFormattedDate", () => {
  it("returns the correct formatted date string", () => {
    const dateTime = "2022-01-01";
    const expected = "Sat, January 1, 2022, 12:00 AM";

    expect(getFormattedDate(dateTime)).toBe(expected);
  });
});

describe("getFormattedTime", () => {
  it("returns the correct formatted date string", () => {
    const dateTime = "2022-01-01";
    const expected = "12:00 AM";

    expect(getFormattedTime(dateTime)).toBe(expected);
  });
});
