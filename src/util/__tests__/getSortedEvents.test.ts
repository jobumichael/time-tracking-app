import { getSortedEvents } from "../getSortedEvents";

describe("getSortedEvents", () => {
  it("returns an array of events sorted by start time", () => {
    const events = [
      {
        id: "1",
        eventName: "Event 1",
        startTime: "2022-01-01T10:00:00Z",
        endTime: "2022-01-01T12:00:00Z",
        description: "Sample description",
        color: "blue",
      },
      {
        id: "2",
        eventName: "Event 2",
        startTime: "2022-01-01T08:00:00Z",
        endTime: "2022-01-01T09:00:00Z",
        description: "Sample description",
        color: "blue",
      },
      {
        id: "3",
        eventName: "Event 3",
        startTime: "2022-01-01T13:00:00Z",
        endTime: "2022-01-01T14:00:00Z",
        description: "Sample description",
        color: "blue",
      },
    ];

    const expected = [
      {
        id: "2",
        eventName: "Event 2",
        startTime: "2022-01-01T08:00:00Z",
        endTime: "2022-01-01T09:00:00Z",
        description: "Sample description",
        color: "blue",
      },
      {
        id: "1",
        eventName: "Event 1",
        startTime: "2022-01-01T10:00:00Z",
        endTime: "2022-01-01T12:00:00Z",
        description: "Sample description",
        color: "blue",
      },
      {
        id: "3",
        eventName: "Event 3",
        startTime: "2022-01-01T13:00:00Z",
        endTime: "2022-01-01T14:00:00Z",
        description: "Sample description",
        color: "blue",
      },
    ];

    const actual = getSortedEvents(events);
    expect(actual).toEqual(expected);
  });
});
