import { validateFormData } from "../validateFormData";

describe("validateFormData", () => {
  it("returns true if eventName is falsy", () => {
    const formData = {
      eventName: "",
      startTime: "2022-01-01T10:00:00Z",
      endTime: "2022-01-01T12:00:00Z",
    };
    const result = validateFormData(formData);

    expect(result).toBe(true);
  });

  it("returns true if startTime is falsy", () => {
    const formData = {
      eventName: "Test Event",
      startTime: null,
      endTime: "2022-01-01T12:00:00Z",
    };
    const result = validateFormData(formData);

    expect(result).toBe(true);
  });

  it("returns true if endTime is falsy", () => {
    const formData = {
      eventName: "Test Event",
      startTime: "2022-01-01T10:00:00Z",
      endTime: undefined,
    };
    const result = validateFormData(formData);

    expect(result).toBe(true);
  });

  it("returns true if startTime is after endTime", () => {
    const formData = {
      eventName: "Test Event",
      startTime: "2022-01-01T12:00:00Z",
      endTime: "2022-01-01T10:00:00Z",
    };
    const result = validateFormData(formData);

    expect(result).toBe(true);
  });

  it("returns false if all validation checks pass", () => {
    const formData = {
      eventName: "Test Event",
      startTime: "2022-01-01T10:00:00Z",
      endTime: "2022-01-01T12:00:00Z",
    };
    const result = validateFormData(formData);

    expect(result).toBe(false);
  });
});
