import { getRandomId } from "../getRandomId";

describe("getRandomId", () => {
  it("returns a string", () => {
    const id = getRandomId();
    expect(typeof id).toBe("string");
  });

  it("returns a unique ID", () => {
    const ids = new Set<string>();
    for (let i = 0; i < 1000; i++) {
      const id = getRandomId();
      expect(ids.has(id)).toBe(false);
      ids.add(id);
    }
  });
});
