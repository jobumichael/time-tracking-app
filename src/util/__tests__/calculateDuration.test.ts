import { calculateDuration } from "../calculateDuration";

describe("calculateDuration", () => {
  it("returns 'N/A' if either startDateTimeStr or endDateTimeStr is null", () => {
    expect(calculateDuration(null, new Date())).toBe("N/A");
    expect(calculateDuration(new Date(), null)).toBe("N/A");
    expect(calculateDuration(null, null)).toBe("N/A");
  });

  it("returns 'N/A' if the duration is 0", () => {
    const startDateTimeStr = new Date("2022-01-01T10:00:00Z");
    const endDateTimeStr = new Date("2022-01-01T10:00:00Z");

    expect(calculateDuration(startDateTimeStr, endDateTimeStr)).toBe("N/A");
  });

  it("returns the correct duration in hours and minutes", () => {
    const startDateTimeStr = new Date("2022-01-01T10:00:00Z");
    const endDateTimeStr = new Date("2022-01-01T12:30:00Z");

    expect(calculateDuration(startDateTimeStr, endDateTimeStr)).toBe(
      "2 hr 30 min"
    );
  });

  it("returns the correct duration in minutes only", () => {
    const startDateTimeStr = new Date("2022-01-01T10:00:00Z");
    const endDateTimeStr = new Date("2022-01-01T10:30:00Z");

    expect(calculateDuration(startDateTimeStr, endDateTimeStr)).toBe("30 min");
  });

  it("returns the correct duration in hours only", () => {
    const startDateTimeStr = new Date("2022-01-01T10:00:00Z");
    const endDateTimeStr = new Date("2022-01-01T12:00:00Z");

    expect(calculateDuration(startDateTimeStr, endDateTimeStr)).toBe("2 hr");
  });
});
