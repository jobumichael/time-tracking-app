import { getLocalStorageItem, setLocalStorageItem } from "../localStorage";

describe("localStorage", () => {
  beforeEach(() => {
    localStorage.clear();
  });

  describe("setLocalStorageItem", () => {
    it("sets an item in localStorage", () => {
      const key = "testKey";
      const value = { foo: "bar" };

      setLocalStorageItem(key, value);
      expect(localStorage.getItem(key)).toBe(JSON.stringify(value));
    });
  });

  describe("getLocalStorageItem", () => {
    it("gets an item from localStorage", () => {
      const key = "testKey";
      const value = { foo: "bar" };
      localStorage.setItem(key, JSON.stringify(value));
      const result = getLocalStorageItem<typeof value>(key);

      expect(result).toEqual(value);
    });

    it("returns null if the item is not found", () => {
      const key = "testKey";
      const result = getLocalStorageItem(key);

      expect(result).toBeNull();
    });

    it("returns null if the item cannot be parsed", () => {
      const key = "testKey";
      localStorage.setItem(key, "invalid JSON");
      const result = getLocalStorageItem(key);

      expect(result).toBeNull();
    });
  });
});
