export const calculateDuration = (
  startDateTimeStr: Date | null,
  endDateTimeStr: Date | null
): string => {
  if (!startDateTimeStr || !endDateTimeStr) {
    return "N/A";
  }
  const startTime: Date = new Date(startDateTimeStr);
  const endTime: Date = new Date(endDateTimeStr);

  const timeDifference: number = endTime.getTime() - startTime.getTime();

  const hours: number = Math.floor(timeDifference / (1000 * 60 * 60));
  const minutes: number = Math.floor((timeDifference / (1000 * 60)) % 60);
  if (hours === 0 && minutes === 0) {
    return "N/A";
  }

  let result = "";
  if (hours > 0) {
    result += `${hours} hr `;
  }
  if (minutes > 0) {
    result += `${minutes} min`;
  }

  return result.trim();
};
