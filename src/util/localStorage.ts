export function setLocalStorageItem<T>(key: string, value: T): void {
  localStorage.setItem(key, JSON.stringify(value));
}

export function getLocalStorageItem<T>(key: string): T | null {
  const storedItem = localStorage.getItem(key);
  if (storedItem) {
    try {
      return JSON.parse(storedItem) as T;
    } catch (error) {
      console.error(`Error parsing localStorage item: ${key}`, error);
      return null;
    }
  }
  return null;
}

export function removeLocalStorageItem(key: string): void {
  localStorage.removeItem(key);
}

export function clearLocalStorage(): void {
  localStorage.clear();
}
