import dayjs from "dayjs";

export const getFormattedDate = (dateTime: string): string => {
  return dayjs(dateTime).format("ddd, MMMM D, YYYY, hh:mm A");
};

export const getFormattedTime = (dateTime: string): string => {
  return dayjs(dateTime).format("hh:mm A");
};
