import dayjs from "dayjs";
import { IEvent } from "interfaces";

export const getSortedEvents = (events: IEvent[]) => {
  const sortedEvents = events.sort((a, b) => {
    return dayjs(a.startTime).isBefore(dayjs(b.startTime)) ? -1 : 1;
  });

  return sortedEvents;
};
