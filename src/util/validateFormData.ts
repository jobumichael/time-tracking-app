import dayjs from "dayjs";

export const validateFormData = (formData: any): boolean => {
  if (!formData.eventName) {
    return true;
  }

  if (!formData.startTime || !formData.endTime) {
    return true;
  }

  if (dayjs(formData.startTime).isAfter(dayjs(formData.endTime))) {
    return true;
  }

  return false;
};
