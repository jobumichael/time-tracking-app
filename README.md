# Time tracking app

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

DEMO : https://time-tracking-app-v1.vercel.app/

## To run the application

In the project directory, you can run:

### `npm install` or `yarn install`

Installs all the dependencies required to run the application.

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

## Features

Below are the features of the application that are implemented.

- Add new event
- Edit event
- Delete event
- View event
- View all events

## Tech Stack

- react,
- react-dom,
- redux
- react-redux
- @reduxjs/toolkit
- react-router-dom,
- typescript
- @mui/icons-material
- @mui/material
- dayjs - **Used dayjs for date manipulation as I am using material ui date picker which requires date to be in dayjs format.**
- @emotion/styled
- @mui/x-date-pickers
- @testing-library/jest-dom
- @testing-library/react


